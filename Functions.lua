function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

function math.clamp(x, min, max)
  return x < min and min or (x > max and max or x)
end

table.indexOf = function( t, object )
    local result
 
    if "table" == type( t ) then
        for i=1,#t do
            if object == t[i] then
                result = i
                break
            end
        end
    end
 
    return result
end

-- Fisher-Yates shuffle from http://santos.nfshost.com/shuffling.html
function shuffle(t)
  for i = 1, #t - 1 do
    local r = math.random(i, #t)
    t[i], t[r] = t[r], t[i]
  end
end
 
-- average of a and b
function avg(a, b)
  return (a + b) / 2
end