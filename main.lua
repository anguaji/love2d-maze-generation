--[[
	Maze generation test - 13 Aug 2016
	by Beth Brennan
]]

require("functions")
require("Maze")

-- SET UP DAT RANDOM
    math.randomseed(os.time())
    math.random()
    math.random()
    math.random()

function love.load()

	love.window.setMode(1024, 720)

	WIDTH = love.graphics.getWidth()
    HEIGHT = love.graphics.getHeight()
    
	pixelUnit = 32
	
	wallSprite = love.graphics.newImage("assets/col-wall-5.png")
	floorSprite = love.graphics.newImage("assets/col-floor-2.png")

	maze_height = 10
	maze_width = 10

	map = generate_maze(maze_width, maze_height)

end

function love.update(dt)

end

function love.draw()

	for x = 0, (WIDTH / pixelUnit) do
		for y = 0, HEIGHT / pixelUnit do
			love.graphics.draw(floorSprite, x * pixelUnit, y * pixelUnit)
			love.graphics.draw(wallSprite, x * pixelUnit, y * pixelUnit)
		end
	end

	for i = 1, maze_height * 2 + 1 do
		for j = 1, maze_width * 2 + 1 do
	    	if map[i][j] then
	        	love.graphics.draw(floorSprite, j * pixelUnit, i * pixelUnit)
	      	end
	    end
	end
	  
end

function love.keypressed(key)
	if(key == " ") then
		map = generate_maze(maze_width, maze_height)
	end
end